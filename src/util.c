/****************************************************************************/
/*                                                                          */
/* ./ascii/util.c   -   Utility procedures                                  */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/


#include "defs.h"
#include "Garmin.h"
#ifdef __LINUX__
#include "time.h"
#endif

/* define global variables */
char gMessageStr[255];
unsigned short product_ID;
unsigned short software_version;
char *protocols;		/* typical contents:
"P000 L001 A010 A100 D108 A201 D202 D108 D210 A301 D310 D301 A500 D501 A600 D600 A700 D700 A800 D800 A900 A902 A903"
 */


/****************************************************************************/
/* Get line from input file - only implemented for compatibility reason.    */
/****************************************************************************/
short
GetLine (FILE * refNum, char *line, short init)
{
  while (!feof (refNum))
    {
      fgets (line, MAX_LINE, refNum);
      if (*line != '#')
	return 1;
    }
  *line = '\0';
  return 0;
}


/****************************************************************************/
/* Print message text.                                                      */
/****************************************************************************/
void
Message (char *txt)
{
  fprintf (stderr, "INFO:  %s\n", txt);
  fflush (stderr);
}


/****************************************************************************/
/* Print error message and exit program.                                    */
/****************************************************************************/
void
Error (char *txt)
{
  fprintf (stderr, "ERROR:  %s\n", txt);
  fflush (stderr);
  exit (1);
}


/****************************************************************************/
/* Get local time in seconds - only implemented for compatibility reason.   */
/****************************************************************************/
long
TickCount ()
{
  time_t count;

  time (&count);
  return (long) count;
}


/****************************************************************************/
/* allocate memory from the heap, checking for out of memory condition      */
/****************************************************************************/
void *
xmalloc (int num)
{
  void *p;

  p = malloc(num);
  if (!p)
    Error("out of memory");
  return p;
}

