/****************************************************************************/
/*                                                                          */
/* ./ascii/graph.c   -   Routines to display bargraph                       */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"


/****************************************************************************/
/* Set BarGraph to a value from 0 to 1  -  not needed in ascii-version.     */
/****************************************************************************/
void
SetBarGraph (double value)
{
}


/****************************************************************************/
/* Init BarGraph window  -  not needed in ascii-version.                    */
/****************************************************************************/
void
InitBarGraph ()
{
}


/****************************************************************************/
/* Close BarGraph window  -  not needed in ascii-version.                   */
/****************************************************************************/
void
CloseBarGraph ()
{
}


/****************************************************************************/
/* Set main window busy  -  not needed in ascii-version.                    */
/****************************************************************************/
void
SetFrameBusy ()
{
}
