/****************************************************************************/
/*                                                                          */
/* main.c   -   Main program and small utility procedures                   */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*    Copyright (c) 2001 by Joao Seabra - CT2GNL (seabra@ci.aac.uc.pt)      */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/




#include "defs.h"
#include "Garmin.h"
#include "Prefs.h"
#include "gpsinfo.h"
#include <signal.h>
#include <sys/time.h>


/* define external variables */
extern struct PREFS gPrefs;	/* Preferences from prefs.c */

/* define global variables */
char cmd = ' ', what = ' ';
char *prog, *progname;
char *dowhat;
char FileName[FILENAME_MAX+1];
FILE *refNum;
enum PROTOCOL mode = GARMIN;
enum FILE_FORMAT file_format = TSV;
int debugging;
int verbose;


/****************************************************************************/
/* Print message if GPS is not responding and exit.                         */
/****************************************************************************/
void
NotResponding ()
{
  fprintf (stderr, "GPS [%s] is not responding - ", gPrefs.Device);
  fprintf (stderr, "make sure it is on and set to GRMN/GRMN protocol.\n");
  exit (1);
}


/****************************************************************************/
/* Print headline with programname, version and copyright notice.           */
/****************************************************************************/
void
PrintHeadLine ()
{
  fprintf (stderr, "%s (ASCII) - Version %s\nCopyright (c) %s by Carsten ",
	   ProgramName, ProgramVersion, ProgramYear);
  fprintf (stderr, "Tschach (tschach@zedat.fu-berlin.de)\n");
  fprintf (stderr,
	   "Linux/KKJ mods by     Janne Sinkkonen <janne@iki.fi> (1996)\n");
  fprintf (stderr,
	   "Copyright (c) 2000 German Grid by Andreas Lange <andreas.lange@rhein-main.de>\n");
  fprintf (stderr,
	   "Copyright (c) 1998,2000 Mayko-mXmap mods by Matthias Kattanek <mattes@ugraf.com>\n");
  fprintf (stderr,
	   "Copyright (c) 2001 Development by Joao Seabra-CT2GNL <seabra@ci.AAC.uc.pt>\n");
  fprintf (stderr,
	   "Copyright (c) 2005 Development by Jim Van Zandt <jrvz@comcast.removeme.net>\n");
}


/****************************************************************************/
/* Print usage information.                                                 */
/****************************************************************************/
void
usage ()
{
  fprintf (stderr, "Usage: %s flag [filename]\n\n", progname);

  fprintf (stderr, 
	   "Flags are:  -v   version:   information about program\n"
	   "            --help          print this help text\n"
	   "            --debug         add debug printouts\n"
	   "            --verbose       increase verbosity\n"
	   "            -p   port:      set serial I/O-Device\n"
	   "            -s   setup:     set datum, format, offset "
	   "device and model\n"
  /*
	   "            -z   model:     set model etrex (or similar model)\n\n"
  */
	   "            -i   ident:     identify connected GPS\n"
	   "            -o   off:       Turn off GPS Device\n"
	   "            -t   time:      Get time from GPS (-ts "
	   "will set time on host)\n\n"
	   "            -d?  download:  r = route, t = track, w = "
	   "waypoint, a = almanac\n"
	   "            -u?  upload:    r = route, t = track, w = "
	   "waypoint\n\n"
	   "            -m   format:    data in Mayko-mXmap format\n\n"
	   "If no filename is entered, data will be written to stdout "
	   "and read from stdin.\n"
	   "Serial I/O-Device can also be set with the environment "
	   "variable $GPSDEV.\n");
  exit (1);
}


/****************************************************************************/
/* Print error messages.                                                    */
/****************************************************************************/
void
ParseError (err)
     int err;
{
  switch (err)
    {
    case 1:
      fprintf (stderr, "You have to specify at least one parameter.\n\n");
      fprintf (stderr, "Start %s without any parameters to get usage ",
	       progname);
      fprintf (stderr, "information.\n");
      break;
    case 2:
      fprintf (stderr, "You have to specify one of '-da -dr -dt -dw'\n\n");
      fprintf (stderr, "Start %s without any parameters to get usage ",
	       progname);
      fprintf (stderr, "information.\n");
      break;
    case 3:
      fprintf (stderr, "You have to specify one of '-ur -ut -uw'\n\n");
      fprintf (stderr, "Start %s without any parameters to get usage ",
	       progname);
      fprintf (stderr, "information.\n");
      break;
    case 4:
      fprintf (stderr,
	       "You have to specify a device name when using '-p'\n\n");
      fprintf (stderr, "Start %s without any parameters to get usage ",
	       progname);
      fprintf (stderr, "information.\n");
      break;
    case 5:
      fprintf (stderr, "Can't get time from GPS Receiver\n");
      break;
    }
  fflush (stderr);
  exit (1);
}


/****************************************************************************/
/* Print verbose version information and copyright notice.                  */
/****************************************************************************/
void
HandleAbout ()
{
  fprintf (stderr, "Parts of this program were taken from MacGPS by %s",
	   "John F. Waers\n\n");

  fprintf (stderr,
	   "If you've any questions or bugs please send email to:\n\n");
  fprintf (stderr, "%53s", "tschach@zedat.fu-berlin.de");
  fprintf (stderr, "\n\n");
  fprintf (stderr, "The newest version can always be found at:\n\n");
  fprintf (stderr, "%57s", "ftp.fu-berlin.de:/pub/unix/misc/gps");
  fprintf (stderr, "\n\n");

  fprintf (stderr,
	   "*******************************************************************************\n");
  fprintf (stderr,
	   "*   Permission to use, copy, modify, and distribute this software and its     *\n");
  fprintf (stderr,
	   "*   documentation for non-commercial purpose, is hereby granted without fee,  *\n");
  fprintf (stderr,
	   "*   providing that the copyright notice appears in all copies and that both   *\n");
  fprintf (stderr,
	   "*   the copyright notice and this permission notice appear in supporting      *\n");
  fprintf (stderr,
	   "*   documentation. I make no representations about the suitability of this    *\n");
  fprintf (stderr,
	   "*   software for any purpose. It is provides \"as is\" without express or       *\n");
  fprintf (stderr,
	   "*   implied warranty.                                                          *\n");
  fprintf (stderr,
	   "*******************************************************************************\n");
  exit (0);
}


/****************************************************************************/
/* Query GPS for device type and software version.                          */
/****************************************************************************/
void
InfoAboutGPS ()
{
  int status;
  char *string;
  status = getGPSVersion(&string);
  printf ("Connected GPS [%s] is: %s\n", gPrefs.Device, string);
  exit (status);
}


/****************************************************************************/
/* Turn off GPS device.                                                     */
/****************************************************************************/
void
TurnOffGPS ()
{
  sendGPSOff ();
  exit (0);
}


/****************************************************************************/
/* Get time from GPS - compare with local time and adjust if requested.     */
/****************************************************************************/
void
AdjustTime (char dowhat, int m)
{
  long diffis;
  time_t clock, clockgps;
  struct timeval tp;

  time (&clock);
  clockgps = getGPSTime (m);
  if (clockgps == -1)
    ParseError (5);

  printf ("Local time determine from GPS-Receiver is:  %s",
	  ctime (&clockgps));
  printf ("Local time on machine is:                   %s\n", ctime (&clock));

  diffis = (clockgps - clock);
  if (diffis < 0)
    diffis = diffis * (-1);
  printf ("Time difference is %ld seconds.\n", diffis);

  /* if set-option and uid=root then adjust local time */
  if (dowhat == 's')
    {
      if (getuid () == 0)
	{
	  if (diffis <= 7200)
	    {
	      clockgps = getGPSTime (m);
	      tp.tv_sec = clockgps;
	      tp.tv_usec = 5000;	/* to adjust lost time in process */
	      settimeofday (&tp, (struct timezone *) NULL);
	      fprintf (stderr, "\nLocal time set to:  %s", ctime (&clockgps));
	    }
	  else
	    {
	      fprintf (stderr,
		       "\nTime difference is > 2 hours - time not set\n");
	    }
	}
      else
	fprintf (stderr, "\nSorry, only root can set the time.\n");
    }

  exit (0);
}


/*****************************************************************************/
/* Catch signals to get grateful death.                                      */
/*****************************************************************************/
void
TerminateHandler (sig)
     int sig;
{
  if (sig == SIGINT)
    {
      fprintf (stderr, "\nDon't touch me...but you've pressed CTRL-C\n");
      fprintf (stderr, "It was your choice....exiting\n");
    }
  else
    {
      fprintf (stderr,
	       "\nGotcha...somebody try to shoot me...but he missed !\n");
      fprintf (stderr, "\nBut okay, committing suicide....<argh!>\n");
    }
  exit (0);
}



/*****************************************************************************/
/*                        H a u p t p r o g r a m m                          */
/*****************************************************************************/
int
main (argc, argv)
     int argc;
     char *argv[];
{
  /* Initialize Signal handler */
  signal (SIGTERM, TerminateHandler);
  signal (SIGINT, TerminateHandler);

  /* Get program name and print headline */
  progname = argv[0];
  PrintHeadLine ();

  /* Print usage information if no parameters where given */
  if (argc <= 1)
    usage ();

  /* Init preferences - overwrite serial device if $GPSDEV is set */
  FileName[0] = '\0';
  InitPrefs ();
  if (getenv ("GPSDEV") != NULL)
    sprintf (gPrefs.Device, "%s", getenv ("GPSDEV"));

  /* Parse arguments */
  for (prog = *argv++; --argc; argv++)
    {
      if (strstr(*argv, "--debug"))
	debugging++;
      else if (0 == strcmp("--verbose",*argv))
	verbose++;
      else if (0 == strcmp("--help",*argv))
	usage();
      else if (argv[0][0] == '-')
	{
	  switch (argv[0][1])
	    {
	    case 'd':
	      dowhat = *argv + 2;
	      cmd = 'd';
	      break;
	    case 'u':
	      dowhat = *argv + 2;
	      cmd = 'u';
	      break;
	    case 'p':
	      if (argv[0][2])	/* device is part of this switch */
		sprintf (gPrefs.Device, "%s", *argv + 2);
	      else		/* device is in next argument */
		if (--argc)
		  sprintf (gPrefs.Device, "%s", *++argv);
		else
		  usage();
	      break;
	    case 'v':
	      HandleAbout ();
	      break;
	    case 'i':
	      InfoAboutGPS ();
	      break;
	    case 'o':
	      TurnOffGPS ();
	      break;
	    case 's':
	      SetupProgram ();
	      break;
	    case 't':
	      dowhat = *argv + 2;
	      AdjustTime (*dowhat, 0);
	      break;

	    case 'm':
	      if (file_format==MAYKO)
		file_format=MAYKO2; /* include rate data */
	      else
		file_format=MAYKO;
	      if (verbose)
		fprintf (stderr, "\n... Mayko-mXmap format is enabled !\n");
	      break;
	    case 'z':
	      gPrefs.model = 'y';
	      break;
	    }
	}
      else
	{
	  if (strlen (FileName) == 0)
	    {
	      FileName[FILENAME_MAX] = 0;
	      strncpy(FileName, argv[0], FILENAME_MAX);
	    }
	}
    }

  /* Exit if no serial device */
  if (strlen (gPrefs.Device) == 0)
    ParseError (4);

  /* Parse up- and download parameters */
  if (cmd != ' ')
    what = dowhat[0];

  switch (cmd)
    {
    case 'd':
      {
	if (strlen (FileName) == 0)
	  refNum = stdout;
	else
	  {
	    refNum = fopen (FileName, "wt");
	    if (!refNum)
	      {
		fprintf(stderr, "Cannot open %s for writing.\n",
			FileName);
		exit(1);
	      }
	  }

	switch (what)
	  {
	  case 'a':
	    getGPSInfo (refNum, ALMANAC);
	    break;
	  case 'r':
	    getGPSInfo (refNum, ROUTE);
	    break;
	  case 't':
	    getGPSInfo (refNum, TRACK);
	    break;
	  case 'w':
	    getGPSInfo (refNum, WAYPOINT);
	    break;
	  default:
	    ParseError (2);
	    break;
	  }
	break;
      }
    case 'u':
      {
	if (strlen (FileName) == 0)
	  refNum = stdin;
	else
	  {
	    refNum = fopen (FileName, "rt");
	    if (!refNum)
	      {
		fprintf(stderr, "Cannot open %s for reading.\n",
			FileName);
		exit(1);
	      }
	  }

	switch (what)
	  {
	  case 'r':
	    sendGPSInfo (refNum, ROUTE);
	    break;
	  case 't':
	    sendGPSInfo (refNum, TRACK);
	    break;
	  case 'w':
	    sendGPSInfo (refNum, WAYPOINT);
	    break;
	  default:
	    ParseError (3);
	    break;
	  }
	break;
      }
    default:
      ParseError (1);
      break;
    }
  exit (0);
}
