/****************************************************************************/
/*                                                                          */
/* ./gps/latlong.c   -   Convert latitude and longitude                     */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"
#include <math.h>

/* define constants */
static const double CONVERT = 11930464.7111111111111;	/* 2^31 / 180 */
#ifndef PI
static const double PI = 3.14159265358979323846;	/* PI */
#endif
static const double WGSa = 6378137.0;	/* WGS84 semimajor axis */
static const double WGSinvf = 298.257223563;	/* WGS84 1/f */
static const short WGS84ID = 100;	/* ID of WGS84 datum */



/****************************************************************************/
/* Converts latitude and longitude in decimal degrees from WGS84 to another */
/* datum or from another datum to WGS84. The arguments to this function     */
/* include a direction flag 'fromWGS84', pointers to double precision       */
/* latitude and longitude, and an index to the gDatum[] array.              */
/****************************************************************************/
void
translate (short fromWGS84, double *latitude, double *longitude,
	   short datumID)
{
  extern struct DATUM const gDatum[];
  extern struct ELLIPSOID const gEllipsoid[];

  double dx = gDatum[datumID].dx;
  double dy = gDatum[datumID].dy;
  double dz = gDatum[datumID].dz;

  double phi = *latitude * PI / 180.0;
  double lambda = *longitude * PI / 180.0;
  double a0, b0, es0, f0;	/* Reference ellipsoid of input data */
  double a1, b1, es1, f1;	/* Reference ellipsoid of output data */
  double psi;			/* geocentric latitude */
  double x, y, z;		/* 3D coordinates with respect to original datum */
  double psi1;			/* transformed geocentric latitude */

  if (datumID == WGS84ID)	/* do nothing if current datum is WGS84 */
    return;

  if (fromWGS84)
    {				/* convert from WGS84 to new datum */
      a0 = WGSa;		/* WGS84 semimajor axis */
      f0 = 1.0 / WGSinvf;	/* WGS84 flattening */
      a1 = gEllipsoid[gDatum[datumID].ellipsoid].a;
      f1 = 1.0 / gEllipsoid[gDatum[datumID].ellipsoid].invf;
    }
  else
    {				/* convert from datum to WGS84 */
      a0 = gEllipsoid[gDatum[datumID].ellipsoid].a;	/* semimajor axis */
      f0 = 1.0 / gEllipsoid[gDatum[datumID].ellipsoid].invf;	/* flattening */
      a1 = WGSa;		/* WGS84 semimajor axis */
      f1 = 1 / WGSinvf;		/* WGS84 flattening */
      dx = -dx;
      dy = -dy;
      dz = -dz;
    }

  b0 = a0 * (1 - f0);		/* semiminor axis for input datum */
  es0 = 2 * f0 - f0 * f0;	/* eccentricity^2 */

  b1 = a1 * (1 - f1);		/* semiminor axis for output datum */
  es1 = 2 * f1 - f1 * f1;	/* eccentricity^2 */

  /* Convert geodedic latitude to geocentric latitude, psi */
  if (*latitude == 0.0 || *latitude == 90.0 || *latitude == -90.0)
    psi = phi;
  else
    psi = atan ((1 - es0) * tan (phi));

  /* Calc x and y axis coordinates with respect to original ellipsoid */
  if (*longitude == 90.0 || *longitude == -90.0)
    {
      x = 0.0;
      y = fabs (a0 * b0 / sqrt (b0 * b0 + a0 * a0 * pow (tan (psi), 2.0)));
    }
  else
    {
      x = fabs ((a0 * b0) /
		sqrt ((1 + pow (tan (lambda), 2.0)) *
		      (b0 * b0 + a0 * a0 * pow (tan (psi), 2.0))));
      y = fabs (x * tan (lambda));
    }

  if (*longitude < -90.0 || *longitude > 90.0)
    x = -x;
  if (*longitude < 0.0)
    y = -y;

  /* Calculate z axis coordinate with respect to the original ellipsoid */
  if (*latitude == 90.0)
    z = b0;
  else if (*latitude == -90.0)
    z = -b0;
  else
    z =
      tan (psi) * sqrt ((a0 * a0 * b0 * b0) /
			(b0 * b0 + a0 * a0 * pow (tan (psi), 2.0)));

  /* Calculate the geocentric latitude with respect to the new ellipsoid */
  psi1 = atan ((z - dz) / sqrt ((x - dx) * (x - dx) + (y - dy) * (y - dy)));

  /* Convert to geodetic latitude and save return value */
  *latitude = atan (tan (psi1) / (1 - es1)) * 180.0 / PI;

  /* Calculate the longitude with respect to the new ellipsoid */
  *longitude = atan ((y - dy) / (x - dx)) * 180.0 / PI;

  /* Correct the resultant for negative x values */
  if (x - dx < 0.0)
    {
      if (y - dy > 0.0)
	*longitude = 180.0 + *longitude;
      else
	*longitude = -180.0 + *longitude;
    }
}


/****************************************************************************/
/* Converts Garmin integer format to double precision decimal degrees.      */
/****************************************************************************/
double
int2deg (long n)
{
  double res;

  res = (double) (n / CONVERT);
  return (res);
}


/****************************************************************************/
/* Converts double precision decimal degrees to Garmin integer format.      */
/****************************************************************************/
long
deg2int (double x)
{
  long res;

  res = (long) (x * CONVERT);
  return (res);
}
