/****************************************************************************/
/*                                                                          */
/* ./gps/garminserial.c   -   Procedure to talk with serial device          */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*    Copyright (c) 2001 by Joao Seabra - CT2GNL (seabra@ci.aac.uc.pt)      */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Prefs.h"
#include "Garmin.h"
#include <sys/types.h>
#include <fcntl.h>
#include <termio.h>
#include <sys/termios.h>

#ifdef SUNOS41
#include <sys/filio.h>
#endif


/* define external variables */
extern struct PREFS gPrefs;

/* define global variables */
int ttyfp;
struct termio ttyset;


/****************************************************************************/
/* Open serial device for requested protocol - only GARMIN supported now.  */
/****************************************************************************/
int
serialOpen (enum PROTOCOL p)
{


  ttyfp = open (gPrefs.Device, O_RDWR);

  /* return when error opening device */
  if (ttyfp < 0)
    return (-1);
  if (ioctl (ttyfp, TCGETA, &ttyset) < 0)
    return (-1);

  /* set baud rate for device */
  switch (p)
    {
    case GARMIN:
      ttyset.c_cflag = CBAUD & B9600;
      ttyset.c_cflag |= CLOCAL;
      break;
    case NMEA:
      ttyset.c_cflag = CBAUD & B4800;
      break;

    default:
      return -1;
    }

  /* set character size and allow to read data */
#ifdef SUNOS41
  ttyset.c_cflag |= (CSIZE & CS8) | CRTSCTS | CREAD;
#else
  ttyset.c_cflag |= (CSIZE & CS8) | CREAD;
#endif

  ttyset.c_iflag = ttyset.c_oflag = ttyset.c_lflag = (ushort) 0;
  ttyset.c_oflag = (ONLRET);

  /* return if unable to set communication parameters */
  if (ioctl (ttyfp, TCSETAF, &ttyset) < 0)
    return (-1);

  return 0;
}


/****************************************************************************/
/* Get number of serial characters available.                               */
/****************************************************************************/
long
serialCharsAvail ()
{
  int count;

  ioctl (ttyfp, FIONREAD, &count);
  return (long) count;
}


/****************************************************************************/
/* Close serial device.                                                     */
/****************************************************************************/
void
serialClose ()
{
  close (ttyfp);
}
