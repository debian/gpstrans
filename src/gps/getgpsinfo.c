/****************************************************************************/
/*                                                                          */
/* ./gps/getgpsinfo.c   -   Receive data from GPS                           */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*    Copyright (c) 1998 by Matthias Kattanek (mattes@ugraf.com)            */
/*                                                                          */
/*    Copyright (c) 2001 by Jo�o Seabra (seabra@ci.aac.uc.pt)               */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"
#include "Prefs.h"
#include "graph.h"
#include "util.h"
#include <sys/types.h>
#include "protocols.h"
#include <time.h>
#include <string.h>
#include <math.h>

/* define external variables */
extern BYTE gGarminMessage[MAX_LENGTH];
extern char *protocols;

/* define global variables */
static short records;
static FILE *FileRefNum;
static char fileData[2*MAX_LINE];
static int AlmanacSat = 0;
char GPSVersionStr[255];

/* function prototypes */
static int xmap_route_nr = 0;
static long xmap_time = 0;
static double xmap_lat = 0;
static double xmap_lon = 0;
static float xmap_speed = 0;
static char *colorCodeToName (int code);
static void doTrackHeader ();

#ifdef OLDCODE
static void doTrack_xmap (void);
static void doTrack (void);
#endif
static void doWaypoint_xmap (void);
static double distance (double lata, double lona, double latb, double lonb);

static void parseGPS (void);
static void doWaypoint (void);
static void doRouteName (void);
static void doAlmanac (void);
static long number (BYTE * p);
static char *string (short n, BYTE * p);
static void FileWrite (char *data);
static float tofloat (unsigned char *p);
static short toshort (unsigned char *p);




/****************************************************************************/
/* Write format information to output file.                                 */
/****************************************************************************/
void
saveFormat (char *fileData, short type)
{
  extern struct PREFS gPrefs;
  extern struct DATUM const gDatum[];
  char format[4];

  time_t thistime;

  switch (gPrefs.format)
    {
    case DMS:
      strcpy (format, "DMS");
      break;
    case DMM:
      strcpy (format, "DMM");
      break;
    default:
      fprintf(stderr, "format code not recognized - using DDD instead\n");
      gPrefs.format = DDD;
    case DDD:
      strcpy (format, "DDD");
      break;
    case UTM:
      strcpy (format, "UTM");
      break;
    case KKJ:
      strcpy (format, "KKJ");
      break;
    case BNG:
      strcpy (format, "BNG");
      break;
    case ITM:
      strcpy (format, "ITM");
      break;
    case SEG:
      strcpy (format, "SEG");
      break;
    case GKK:
      strcpy (format, "GKK");
      break;			/* Gauss Krueger Grid = German Grid */
    }

  switch(file_format)
    {
    case MAYKO:
    case MAYKO2:
      time (&thistime);
      if (type == TRACK)
	sprintf (fileData, "xmaplog 1.0 %s", ctime (&thistime));
      else if (type == ROUTE)
	sprintf (fileData, "xmaproute 1.0 %s", ctime (&thistime));
      else
	sprintf (fileData, "unknown xmap format %s", ctime (&thistime));
      break;

    case TSV:

      /****************************************************************************/
      /*   The following header is used to save information about the data        */
      /*   in the TSV data files. If changed, the function getFileData() in       */
      /*   SendGPSInfo.c must also be changed.                                    */
      /*                                                                          */
      /*   Format: DDD  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS           */
      /*           |||              ||||||            |||                         */
      /*   0         1         2         3         4         5         6          */
      /*   0123456789012345678901234567890123456789012345678901234567890123456789 */
      /****************************************************************************/

      sprintf (fileData, "Format: %s  UTC Offset: %6.2f hrs  Datum[%03d]: %s\n",
	       format, gPrefs.offset, gPrefs.datum, gDatum[gPrefs.datum].name);

      switch (type)
	{
	case ROUTE:
	case WAYPOINT:

	  if (strstr(protocols, "A100 D100"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                     \t");
	  else if (strstr(protocols, "A100 D101"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                     \t");
	  else if (strstr(protocols, "A100 D102"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                     \t");
	  else if (strstr(protocols, "A100 D103"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                     \t");
	  else if (strstr(protocols, "A100 D104"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                     \t");
	  else if (strstr(protocols, "A100 D105"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                             \t");
	  else if (strstr(protocols, "A100 D106"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                             \t");
	  else if (strstr(protocols, "A100 D107"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                     \t");
	  else if (strstr(protocols, "A100 D108"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                             \t"
		   "Altitude\t");
	  else if (strstr(protocols, "A100 D109"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                             \t"
		   "Date       \t"
		   "Altitude\t");
	  else if (strstr(protocols, "A100 D110"))
	    strcat(fileData, 
		   "Type\t"
		   "Name\t"
		   "Comment                                             \t"
		   "Date       \t"
		   "Altitude\t");
	  break;
	case TRACK:
	  if (strstr(protocols, "D300"))
	    strcat(fileData, 
		   "Type\t"
		   "Date               \t");
	  else if (strstr(protocols, "D301"))
	    strcat(fileData, 
		   "Type\t"
		   "Date               \t"
		   "Altitude\t");
	  else if (strstr(protocols, "D302"))
	    strcat(fileData, 
		   "Type\t"
		   "Date               \t"
		   "Altitude\t");
	}

      switch (gPrefs.format)
	{
	case DMS:
	case DMM:
	case DDD: strcat(fileData, "Latitude\tLongitude\t\n"); break;
	case UTM: strcat(fileData, "ZE\tZN\tEasting\tNorthing\t\n"); break;
	case BNG:
	case ITM:
	case KKJ: strcat(fileData, "Zone\tEasting\tNorthing\t\n"); break;
	  /*  case SUI: appears in sendgpsinfo.c comment but not implemented */
	case SEG:
	case GKK: strcat(fileData, "Easting\tNorthing\t\n"); break;
	default: ;
	}
      break;
    }
}


/****************************************************************************/
/* Query GPS for its model and software version.  Set string to point
   to a text string.  Return nonzero status if error.  */
/****************************************************************************/
int getGPSVersion (char **string)
{
  extern struct PREFS gPrefs;
  extern char gMessageStr[];
  char temp[255];
  short err;
  int last = 0, i;
  BYTE *data=gGarminMessage+3;
  extern unsigned short product_ID, software_version;

  *string=gMessageStr;
  SetFrameBusy (1);

  /* First check if GPS responds to the "are-you-ready" package */
  if (CheckGPS ())
    {
      usleep (10000);		/* a fast computer requires this - jrv */
      if ((err = serialOpen (GARMIN)) != noErr)
	{
	  sprintf (gMessageStr, "The port initialization of %s has failed.",
		   gPrefs.Device);
	  Error (gMessageStr);
	  return 1;		/* FAIL */
	}

      if (debugging)
	fprintf(stderr, "getGPSVersion: sending product data request\n");

      /* send request and receive version package */
      sendGPSMessage (gid4, 2);	/* protocol A000, Pid_Product_Rqst */
      getGPSack ();		/* presumably the GPS's ACK packet */

      getGPSMessage ();		/* get the GPS Pid_Product_Data */
      sendGPSMessage (gid5, 4);	/* send ACK packet for GPS response */

      /*
	data returned has this format:
	typedef struct {
	uint16 product_ID;
	sint16 software_version;
	char product_description[]; // null-terminated string
	} Product_Data_Type;
      */	
      product_ID=toshort(data);
      software_version=toshort(data+2);

      /* extract model name and software version */
      sprintf (GPSVersionStr, "Garmin %s", gGarminMessage + 7);
      GPSVersionStr[strlen (GPSVersionStr) - 1] = '\0';
      for (i = 0; i <= strlen (GPSVersionStr); i++)
	{
	  if (GPSVersionStr[i] == ' ')
	    last = i;
	}
      sprintf (temp, " - V%s", &GPSVersionStr[last + 1]);
      GPSVersionStr[last] = '\0';
      while ((i > 0) && (GPSVersionStr[strlen (GPSVersionStr) - 1] == ' '))
	GPSVersionStr[strlen (GPSVersionStr) - 1] = '\0';

      sprintf (GPSVersionStr, "%s%s", GPSVersionStr, temp);

      usleep (10000);		/* wait 1/10 sec for device to send a
				   protocol string */
      if (serialCharsAvail())
	{			/* the device apparently implements
				   the protocol capability protocol */
	  char *p;
	  BYTE *s=data;
	  BYTE tag;
	  unsigned short udata;
	  int n;
	  getGPSMessage();
	  n=data[-1];
	  if (n<0)
	    n=0;
	  p=protocols=xmalloc(5*n/3+1);
	  for ( ; n>0; n-=3)
	    {
	      tag=*s; udata=toshort(s+1); 
	      /* translate each protocol tag and data to the string used in the docs */
	      sprintf(p, " %c%03d", tag, udata);
	      s+=3; p+=5;
	    }
	  *p=0;
	  /*
	    A100 - waypoint transfer
	    A20x - route transfer
	    A30x - track log transfer
	    A400 - proximity waypoint transfer
	    A500 - almanac transfer
	    A600 - Date and time initialization
	    A700 - Position initialization
	    A800 - PVT protocol
	    A906 - lap transfer

	    For an etrex legend:
	    P000 L001 
	    A010 
	    A100 D108 
	    A201 D202 D108 D210 
	    A301 D310 D301 
	    A500 D501 
	    A600 D600 
	    A700 D700 
	    A800 D800 
	    A900 
	    A902 
	    A903
	  */
	}
      else
	{			/* Look up the protocols using the
				   product ID and software version */
	  typedef struct
	  {
	    uint16 product_ID;		/* product ID */
	    sint16 software_version;	/* the earliest software version
					   implementing this list of
					   protocols */
	    char *protocol;		/* list of protocols and their data
					   types */
	  } protocol_t;
	  /* These are protocol capabilities of "many" devices that do not
	     implement the Protocol Capability Protocol, from Table 28 in the
	     interface specification */
	  static protocol_t historic_protocols[] = {
	    {7, 000, "L001 A010 A100 D100 A200 D200 D100 A500 D500 A600 D600 A700 D700"},
	    /*                   waypoints routes         tracks    prox wpt  almanac */
	    {25, 000, "L001 A010 A100 D100 A200 D200 D100 A300 D300 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {13, 000, "L001 A010 A100 D100 A200 D200 D100 A300 D300 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {14, 000, "L001 A010 A100 D100 A200 D200 D100 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {15, 000, "L001 A010 A100 D151 A200 D200 D151 A400 D151 A500 D500 A600 D600 A700 D700"},
	    {18, 000, "L001 A010 A100 D100 A200 D200 D100 A300 D300 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {20, 000, "L002 A011 A100 D150 A200 D201 D150 A400 D450 A500 D550 A600 D600 A700 D700"},
	    {22, 000, "L001 A010 A100 D152 A200 D200 D152 A300 D300 A400 D152 A500 D500 A600 D600 A700 D700"},
	    {23, 000, "L001 A010 A100 D100 A200 D200 D100 A300 D300 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {24, 000, "L001 A010 A100 D100 A200 D200 D100 A300 D300 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {29, 000, "L001 A010 A100 D101 A200 D201 D101 A300 D300 A400 D101 A500 D500 A600 D600 A700 D700"},
	    {29, 400, "L001 A010 A100 D102 A200 D201 D102 A300 D300 A400 D102 A500 D500 A600 D600 A700 D700"},
	    {31, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {33, 000, "L002 A011 A100 D150 A200 D201 D150 A400 D450 A500 D550 A600 D600 A700 D700"},
	    {34, 000, "L002 A011 A100 D150 A200 D201 D150 A400 D450 A500 D550 A600 D600 A700 D700"},
	    {35, 000, "L001 A010 A100 D100 A200 D200 D100 A300 D300 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {36, 000, "L001 A010 A100 D152 A200 D200 D152 A300 D300 A400 D152 A500 D500 A600 D600 A700 D700"},
	    {36, 300, "L001 A010 A100 D152 A200 D200 D152 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {39, 000, "L001 A010 A100 D151 A200 D201 D151 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {41, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {42, 000, "L001 A010 A100 D100 A200 D200 D100 A300 D300 A400 D400 A500 D500 A600 D600 A700 D700"},
	    {44, 000, "L001 A010 A100 D101 A200 D201 D101 A300 D300 A400 D101 A500 D500 A600 D600 A700 D700"},
	    {45, 000, "L001 A010 A100 D152 A200 D201 D152 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {47, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {48, 000, "L001 A010 A100 D154 A200 D201 D154 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {49, 000, "L001 A010 A100 D102 A200 D201 D102 A300 D300 A400 D102 A500 D501 A600 D600 A700 D700"},
	    {50, 000, "L001 A010 A100 D152 A200 D201 D152 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {52, 000, "L002 A011 A100 D150 A200 D201 D150 A400 D450 A500 D550 A600 D600 A700 D700"},
	    {53, 000, "L001 A010 A100 D152 A200 D201 D152 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {55, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {56, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {59, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {61, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {62, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {64, 000, "L002 A011 A100 D150 A200 D201 D150 A400 D450 A500 D551 A600 D600 A700 D700"},
	    {71, 000, "L001 A010 A100 D155 A200 D201 D155 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {72, 000, "L001 A010 A100 D104 A200 D201 D104 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {73, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {74, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A500 D500 A600 D600 A700 D700"},
	    {76, 000, "L001 A010 A100 D102 A200 D201 D102 A300 D300 A400 D102 A500 D501 A600 D600 A700 D700"},
	    {77, 000, "L001 A010 A100 D100 A200 D201 D100 A300 D300 A400 D400 A500 D501 A600 D600 A700 D700"},
	    {77, 301, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {77, 350, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {77, 361, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {87, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {88, 000, "L001 A010 A100 D102 A200 D201 D102 A300 D300 A400 D102 A500 D501 A600 D600 A700 D700"},
	    {95, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {96, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {97, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A500 D501 A600 D600 A700 D700"},
	    {98, 000, "L002 A011 A100 D150 A200 D201 D150 A400 D450 A500 D551 A600 D600 A700 D700"},
	    {100, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {105, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {106, 000, "L001 A010 A100 D103 A200 D201 D103 A300 D300 A400 D403 A500 D501 A600 D600 A700 D700"},
	    {112, 000, "L001 A010 A100 D152 A200 D201 D152 A300 D300 A500 D501 A600 D600 A700 D700"},
	    /* 179 is the etrex Legend, which *usually* follows the
	       protocol protocol, but is included here because it
	       sometimes doesn't */
	    {179, 000, "L001 A010 A100 D108 A201 D202 D108 D210 A301 D310 D301 A500 D501 A600 D600 A700 D700 A800 D800 A900 A902 A903"},
	    {0}
	  };
	  protocol_t *p=historic_protocols, *next;

	  if (verbose)
	    printf("checking historic protocols for product ID %d\n", product_ID);

	  while(p->product_ID)
	    {
	      if (p->product_ID == product_ID)
		while(1)
		  {
		    next = p+1;
		    if ((next->product_ID != product_ID) ||
			(software_version < next->software_version))
		      goto found_protocol;
		    p++;
		  }
	      p++;
	    }
	  printf ("Warning: device with product ID %d is unknown"
		  " - assuming it's like a GPS II.\n", product_ID);
	  p=historic_protocols+31;
	  
	found_protocol:
	  protocols = p->protocol;
	}
      usleep (60000);		/* 5000 < needed < 6000 */

      if (debugging)
	printf("product ID 0x%02x=%d, software version 0x%04x=%d, protocol string: \n%s\n",
	       product_ID, product_ID, software_version, software_version, protocols);

      serialClose ();

      SetFrameBusy (0);
    }
  else
    {
      sprintf (GPSVersionStr, "<GPS not responding>");

      SetFrameBusy (0);
      *string=GPSVersionStr;
      return 1;			/* FAIL */
    }
  *string=GPSVersionStr;
  return 0;			/* normal return */
}


/****************************************************************************/
/* Query GPS for current GPS time (in GMT/UTC).                             */
/****************************************************************************/
long
getGPSTime (int var)
{
  extern struct PREFS gPrefs;
  extern char gMessageStr[];
  short err;
  struct tm *txx;
  time_t clock;


  /* Check if GPS responds to the "are-you-ready" package */
  if (!CheckGPS ())
    NotResponding ();

  /* open serial device */
  if ((err = serialOpen (GARMIN)) != noErr)
    {
      sprintf (gMessageStr, "The port initialization of %s has failed.",
	       gPrefs.Device);
      Error (gMessageStr);
      return (0);
    }

  if ((gPrefs.model == 'y'))
    {
      /* send command and receive package */
      sendGPSMessage (tim1, 4);
      getGPSack ();
      getGPSMessage ();

    }
  else
    {
      /* send command and receive package */
      sendGPSMessage (tim1, 4);
      getGPSack ();		/* this is the ACK-package */
      getGPSMessage ();		/* This is of the unknown type 09: leap seconds ? */
      getGPSack ();		/* This is ACK again */
      getGPSMessage ();		/* Stuff */
    }




  /* close serial device */
  serialClose ();

  /* exit if not a valid time package */

  if (*(gGarminMessage + 1) != 0x0e)
    return (-1);

  /* convert UTC time to localtime using the unix timezone */
  time (&clock);
  txx = gmtime (&clock);


  txx->tm_sec = *(gGarminMessage + 10);
  txx->tm_min = *(gGarminMessage + 9);
  txx->tm_hour = *(gGarminMessage + 7);
  txx->tm_year = (toshort (gGarminMessage + 5) - 1900);
  txx->tm_mday = *(gGarminMessage + 4);
  txx->tm_mon = (*(gGarminMessage + 3)) - 1;

  clock = timegm (txx);
  return (clock);
}


/****************************************************************************/
/* Get multiple line packages from GPS (route, waypoint, track)             */
/****************************************************************************/
void
getGPSInfo (FILE * refNum, short type)
{
  extern struct PREFS gPrefs;
  extern char gMessageStr[];

  short total;
  short done = 0;
  BYTE *init, *req;
  short err;
  char *rType;
  int junk;
  char *junk_str;

  /* First check if GPS responds to the "are-you-ready" package */
  if (!CheckGPS ())
    NotResponding ();

  junk=getGPSVersion(&junk_str); /* load the "protocols" array */

  /* open serial device */
  if ((err = serialOpen (GARMIN)) != noErr)
    {
      sprintf (gMessageStr, "The port initialization of %s has failed.",
	       gPrefs.Device);
      Error (gMessageStr);
      return;
    }

  FileRefNum = refNum;
  records = 0;

  /* Save output format if not Almanac-Data */
  saveFormat (fileData, type);
  if (type != ALMANAC)
    FileWrite (fileData);

  /* process different requests */
  switch (type)
    {
    default:
    case ALMANAC:  init = alm1; req = alm2; rType = "almanac";  break;
    case ROUTE:    init = rte1; req = rte2; rType = "routes";   break;
    case TRACK:    init = trk1; req = trk2; rType = "track";    break;
    case WAYPOINT: init = wpt1; req = wpt2; rType = "waypoint"; break;
    }

  AlmanacSat = 0;

  /* open BarGraph window */
  InitBarGraph ();

  /* send init package */
  sendGPSMessage (init, 4);
  getGPSack();

  /* exit if no package received from GPS */
  if (getGPSMessage () == 0)
    {
      CloseBarGraph ();
      serialClose ();
      return;
    }
  total = toshort(gGarminMessage+3); /* get number of records to
					transfer */
  if (verbose)
    {
      sprintf(gMessageStr, "GPS receiver will send %d packets.",
	      total);
      Message (gMessageStr);
    }
  if (total < 1)
    total = 1;			/* prevent divide by zero or negative
				   length progress bar */
  
  /* repeat until all packages received */
  while (!done)
    {
      sendGPSMessage (req, 4);
      if (getGPSMessage () == 0)
	break;
      done = (gGarminMessage[1] == 0x0c);
      if (!done)
	{
	  parseGPS ();
	  SetBarGraph ((double) (records) / (double) total);
	}
    }

  /* close BarGraph and serial device */
  CloseBarGraph ();
  serialClose ();
  if (verbose || debugging)
    {
      sprintf (gMessageStr,
	       "%d %s packets were transferred from the GPS receiver.",
	       records, rType);
      Message (gMessageStr);
      if (debugging && total && (records != total))
	{
	  sprintf(gMessageStr, " (%d were expected.)", total);
	  Message (gMessageStr);
	}
    }
}


/****************************************************************************/
/* Parse messages received from GPS.                                        */
/****************************************************************************/
static void
parseGPS ()
{
  switch (gGarminMessage[1])
    {

    case RTE_NAM:		/* Route name record */
      doRouteName ();
      break;
    case RTE_WPT:		/* Route waypoint record */
      if (file_format == MAYKO)
	doWaypoint_xmap ();
      else
	doWaypoint ();
      break;
    case ALM:			/* Almanac record */
      doAlmanac ();
      break;
    case Pid_Trk_Hdr:
      doTrackHeader ();
      break;
    case TRK:			/* Track record */
      if (file_format == MAYKO)
	//	doTrack_xmap ();
	doWaypoint ();
      else
	//	doTrack ();
	doWaypoint ();
      break;
    case WPT:			/* Waypoint record */
      doWaypoint ();
      break;
    default:
      if (debugging)
	printf("parseGPS: unexpected package, packet ID 0x%02X=%d\n",
	       gGarminMessage[1], gGarminMessage[1]);
      return;
    }
  records++;
}


/****************************************************************************/
/* Process RouteName package (data type D200, D201, or D202) and write
   to output file\.  */
/****************************************************************************/
static void
doRouteName ()
{
  typedef struct
  {
    unsigned int nmbr;
    char *cmnt;
  } route_name_type;
  route_name_type rte;

  rte.cmnt=0;
  rte.nmbr=~1;

  if (strstr(protocols, "D200"))
    {
      char *d=(char *)(gGarminMessage+3);
      rte.nmbr=d[0];
      /* no string field */
    }

  else if (strstr(protocols, "D201"))
    {
      D201_Rte_Hdr_Type *d=(D201_Rte_Hdr_Type *)(gGarminMessage+3);
      
      rte.nmbr=d->nmbr;
      rte.cmnt=(char *)(&d->cmnt);
    }
  else if (strstr(protocols, "D202"))
    {
      D202_Rte_Hdr_Type *d=(D202_Rte_Hdr_Type *)(gGarminMessage+3);
      
      /* no route number */
      rte.cmnt=(char *)(&d->rte_ident);
    }
  
  if (file_format == MAYKO)
    {
      if (rte.nmbr != ~1)
	xmap_route_nr = rte.nmbr;
      else
	xmap_route_nr = 0;	/* punt */
    }
  else
    {
      sprintf (fileData, "R\t");
      FileWrite (fileData);
      if (rte.nmbr != ~1)
	{
	  sprintf (fileData, "%d\t", rte.nmbr);
	  FileWrite (fileData);
	}
      if (rte.cmnt)
	{
	  sprintf (fileData, "%s\n", rte.cmnt);
	  FileWrite (fileData);
	}
    }
}

static void
doTrackHeader ()
{
  struct
  {
    unsigned char dspl;
    unsigned char color;
    char *ident;
    int index;
    struct {
      unsigned dspl:1;
      unsigned color:1;
      unsigned ident:1;
      unsigned index:1;
    } has;
  } hdr;

  hdr.has.dspl =
    hdr.has.color =
    hdr.has.ident =
    hdr.has.index = 0;

  if (strstr(protocols, "D310"))
    {
      D310_Trk_Hdr_Type *d=(D310_Trk_Hdr_Type *)(gGarminMessage+3);
      hdr.dspl = d->dspl;
      hdr.color = d->color;	/* same as in D108 */
      hdr.ident = (char *)(&d->trk_ident);
      hdr.has.dspl = hdr.has.color = hdr.has.ident = 1;
    }
  if (strstr(protocols, "D311"))
    {
      D311_Trk_Hdr_Type *d=(D311_Trk_Hdr_Type *)(gGarminMessage+3);
      hdr.index = toshort ((BYTE *)(&d->index));
      hdr.has.index = 1;
    }
  if (strstr(protocols, "D312"))
    {
      D312_Trk_Hdr_Type *d=(D312_Trk_Hdr_Type *)(gGarminMessage+3);
      hdr.dspl = d->dspl;
      hdr.color = d->color;	/* same as in D110 */
      hdr.ident = (char *)(&d->trk_ident);
      hdr.has.dspl = hdr.has.color = hdr.has.ident = 1;
    }

  switch(file_format)
    {
    case MAYKO:
    case MAYKO2:
      break;
    case TSV:
      if (hdr.has.dspl)
	printf("H\t%s\t%s\t%.51s\n",
	       hdr.dspl?"display":"hide",
	       colorCodeToName(hdr.color),
	       hdr.ident);
      else
	printf("H\t%d\n", hdr.index);
      break;
    }  
}

/* return a pointer to a static string with the name of the color
   corresponding to CODE. */
static char *
colorCodeToName (int code)
{
  char *color_name[18]={
    "black",
    "dark red",
    "dark green",
    "dark yellow",
    "dark blue",
    "dark magenta",
    "dark cyan",
    "light gray",
    "dark gray",
    "red",
    "green",
    "yellow",
    "blue",
    "magenta",
    "cyan",
    "white",
    "transparent",
    "default"
  };
  if (code == 0x1f || code == 255)
    return color_name[17];
  if (code < 0 || code > 16)
    {
      if (debugging)
	printf("colorCodeToName: unexpected color code 0x%02X=%d, "
	       "assuming black\n", code, code);
      code = 0;
    }      
  return color_name[code];
}


#ifdef OLDCODE
/****************************************************************************/
/* Process Track package and write to output file\.                        */
/****************************************************************************/
static void
doTrack ()
{
  extern struct PREFS gPrefs;

  double latitude, longitude, x, y;
  char zone[6];

  if (*(gGarminMessage + 15) == '\x01')	/* New track, create blank record */
    if (records != 0)
      FileWrite ("\n");

  /* convert bytes to latitude / longitude */
  latitude = int2deg (number (gGarminMessage + 3));
  longitude = int2deg (number (gGarminMessage + 7));

  /* translate to selected datum */
  translate (1, &latitude, &longitude, gPrefs.datum);

  /* Write track time to file */
  sprintf (fileData, "T\t%s\t", secs2dt (number (gGarminMessage + 11),
					 gPrefs.offset));
  FileWrite (fileData);

  /* convert to selected position format */
  switch (gPrefs.format)
    {
    case DMS:
      sprintf (fileData, "%s\t", toDMS (latitude));
      FileWrite (fileData);
      sprintf (fileData, "%s\n", toDMS (longitude));
      FileWrite (fileData);
      break;
    case DMM:
      sprintf (fileData, "%s\t", toDM (latitude));
      FileWrite (fileData);
      sprintf (fileData, "%s\n", toDM (longitude));
      FileWrite (fileData);
      break;
    case DDD:
      sprintf (fileData, "%03.7f\t%04.7f\n", latitude, longitude);
      FileWrite (fileData);
      break;
    case UTM:			/* Convert to UTM grid */
      DegToUTM (latitude, longitude, zone, &x, &y);
      sprintf (fileData, "%s\t%07.0f\t%07.0f\n", zone, x, y);
      FileWrite (fileData);
      break;
    case KKJ:			/* Convert to KKJ grid */
      DegToKKJ (latitude, longitude, zone, &x, &y);
      sprintf (fileData, "%s\t%07.0f\t%07.0f\n", zone, x, y);
      FileWrite (fileData);
      break;
    case BNG:			/* Convert to British grid */
      DegToBNG (latitude, longitude, zone, &x, &y);
      sprintf (fileData, "%s\t%05.0f\t%05.0f\n", zone, x, y);
      FileWrite (fileData);
      break;
    case ITM:			/* Convert to Irish grid */
      DegToITM (latitude, longitude, zone, &x, &y);
      sprintf (fileData, "%s\t%05.0f\t%05.0f\n", zone, x, y);
      FileWrite (fileData);
      break;
    case SEG:			/* Convert to Swedish grid */
      DegToSEG (latitude, longitude, &x, &y);
      sprintf (fileData, "%07.0f\t%07.0f\n", x, y);
      FileWrite (fileData);
      break;
    case GKK:
      DegToGKK (latitude, longitude, &x, &y);
      sprintf (fileData, "%07.0f\t%07.0f\n", x, y);
      FileWrite (fileData);
      break;
    default:
      break;
    }
}

/****************************************************************************/
/* convert Track package and write to Mayko xmap log                        */
/****************************************************************************/
static void
doTrack_xmap ()
{
  extern struct PREFS gPrefs;

  double latitude, longitude;

  int altitude = 0;
  long t_diff = 0;
  double x_dist = 0;
  float x_speed = 0;

  if (*(gGarminMessage + 15) == '\x01')
    {				/* New track, create blank record */
      xmap_lat = 0;		// reset xmap lat + lon
      xmap_lon = 0;
      xmap_time = 0;
      if (records != 0)
	FileWrite ("\n");
    }

  /* convert bytes to latitude / longitude */
  latitude = int2deg (number (gGarminMessage + 3));
  longitude = int2deg (number (gGarminMessage + 7));

  /* translate to selected datum */
  translate (1, &latitude, &longitude, gPrefs.datum);

  /*
   * --- calculate speed
   */

  if (xmap_lat)
    {
      x_dist = distance (latitude, longitude, xmap_lat, xmap_lon);
      t_diff = number (gGarminMessage + 11) - xmap_time;
      if (t_diff)
	x_speed = (float) ((x_dist / (double) t_diff) * 3600 / 1.852);
      else
	x_speed = xmap_speed;
    }

  /*
   * --- write mayko xmap format 
   */

  sprintf (fileData, "1 %03.7f %04.7f ", latitude, longitude);
  FileWrite (fileData);

  sprintf (fileData, "%.1f %d ", x_speed, altitude);
  FileWrite (fileData);

  sprintf (fileData, "%s", secs2dt (number (gGarminMessage + 11),
				    gPrefs.offset));
  FileWrite (fileData);

  if (xmap_lat)
    {
      if (file_format == MAYKO)
	{
	  sprintf (fileData, " x%03.7f %04.7f", xmap_lat, xmap_lon);
	  FileWrite (fileData);
	  sprintf (fileData, " %06.7f", x_dist);
	  FileWrite (fileData);
	  sprintf (fileData, " -%d-", (int) t_diff);
	  FileWrite (fileData);
	  sprintf (fileData, " %.3f", x_speed);
	  FileWrite (fileData);
	}
    }

  FileWrite ("\n");

  xmap_lat = latitude;		//save lat + lon for next round
  xmap_lon = longitude;
  xmap_time = number (gGarminMessage + 11);
  xmap_speed = x_speed;

}
#endif /* OLDCODE */


/****************************************************************************/
/* convert Track package and write to Mayko xmap log                        */
/****************************************************************************/

static void
doWaypoint_xmap ()
{
  extern struct PREFS gPrefs;

  double latitude, longitude;

  printf ("route number %d\n", xmap_route_nr);

  if (xmap_route_nr)		// save only "active" route 0
    return;

  /* convert bytes to latitude / longitude */
  latitude = int2deg (number (gGarminMessage + 9));
  longitude = int2deg (number (gGarminMessage + 13));

  /* translate to selected datum */
  translate (1, &latitude, &longitude, gPrefs.datum);

  /*
   * --- write mayko xmap route format 
   */

  sprintf (fileData, "%s,", string (6, gGarminMessage + 3));
  FileWrite (fileData);

  sprintf (fileData, "%03.6f,%04.6f,0,,\n", latitude, longitude);
  FileWrite (fileData);

}

/* return distance in km from (lata,lona) to (latb,lonb), where the
   latter are in degrees.  Uses spherical earth approximation. */
double
distance (double lata, double lona, double latb, double lonb)
{
#define DEG2RAD         (3.14159265358979323846E0/180.0)
  double l0 = lona * DEG2RAD;
  double l1 = lonb * DEG2RAD;
  double b0 = lata * DEG2RAD;
  double b1 = latb * DEG2RAD;

  return (6371*2*asin(sqrt(cos(b1)*cos(b0)*sin(0.5*(l1 - l0))*sin(0.5*(l1 - l0))
			   + sin(0.5*(b1 - b0))*sin(0.5*(b1 - b0)))));
}

/****************************************************************************/
/* Process Waypoint packet (D1xx), Route packet (D2xx), or Track packet (D3xx)
   and write to output file.  */
/****************************************************************************/
static void
doWaypoint ()
{
  extern struct PREFS gPrefs;
  char zone[6], *s;
  double x,y;
  double x_dist=0, t_diff=0, x_speed=0;

  /* We extract these from the packet if available */
  struct 
  {
    double latitude, longitude;
    double altitude;
    double seconds;
    char name[16];
    char *comment;
    char new_trk;
    struct {
      /* latitude and longitude are always present */
      unsigned altitude:1;
      unsigned seconds:1;
      unsigned name:1;
      unsigned comment:1;
    } has;
  } pkt;
  char flag;
  char Pid;

  pkt.new_trk =
    pkt.has.altitude =
    pkt.has.seconds =
    pkt.has.name =
    pkt.has.comment = 0;

  Pid = gGarminMessage[1];

  switch(Pid)
    {
    case Pid_Rte_Wpt_Data:
    case Pid_Wpt_Data:
      flag='W';

      /* all the devices seem to use the same packet type for
	 waypoints and routes, but something else for tracks */
      if (strstr(protocols, "D100"))
	{
	  D100_Wpt_Type *d=(D100_Wpt_Type *)(gGarminMessage+3);
	  /* D100_Wpt_Type:
	     struct {
	     char ident[6];	// identifier
	     position_type posn;   // position
	     uint32 unused;        // should be set to zero
	     char cmnt[40];        // comment
	     }
	  */
	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-6.6s",(char *)(&d->ident));
	  pkt.comment=(char *)(&d->cmnt);
	  pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D101"))
	{
	  D101_Wpt_Type *d=(D101_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-6.6s",(char *)(&d->ident));
	  pkt.comment=(char *)(&d->cmnt);
	  pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D102"))
	{
	  D102_Wpt_Type *d=(D102_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-6.6s",(char *)(&d->ident));
	  pkt.comment=(char *)(&d->cmnt);
	  pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D103"))
	{
	  D103_Wpt_Type *d=(D103_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-6.6s",(char *)(&d->ident));
	  pkt.comment=(char *)(&d->cmnt);
	  pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D104"))
	{
	  D104_Wpt_Type *d=(D104_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-6.6s",(char *)(&d->ident));
	  pkt.comment=(char *)(&d->cmnt);
	  pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D105"))
	{
	  D105_Wpt_Type *d=(D105_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-15.15s",(char *)(&d->ident));
	  /* no comment */
	  pkt.has.name=1;
	}
      else if (strstr(protocols, "D106"))
	{
	  D106_Wpt_Type *d=(D106_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-15.15s",(char *)(&d->ident));
	  /* no comment */
	  pkt.has.name=1;
	}
      else if (strstr(protocols, "D107"))
	{
	  D107_Wpt_Type *d=(D107_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  sprintf(pkt.name, "%-6.6s",(char *)(&d->ident));
	  pkt.comment=(char *)(&d->cmnt);
	  pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D108"))
	{
	  D108_Wpt_Type *d=(D108_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  pkt.altitude = tofloat ((BYTE *)(&d->alt));

	  s=(char *)(char *)(&d->ident);

	  sprintf(pkt.name, "%-15.15s",s);
	  s+=strlen(s)+1;

	  pkt.comment=s;
	  pkt.has.altitude=pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D109"))
	{
	  D109_Wpt_Type *d=(D109_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  pkt.altitude = tofloat ((BYTE *)(&d->alt));

	  s=(char *)(char *)(&d->ident);

	  sprintf(pkt.name, "%-15.15s",s);
	  s+=strlen(s)+1;

	  pkt.comment=s;
	  pkt.has.altitude=pkt.has.name=pkt.has.comment=1;
	}
      else if (strstr(protocols, "D110"))
	{
	  D110_Wpt_Type *d=(D110_Wpt_Type *)(gGarminMessage+3);

	  /* convert bytes to latitude/longitude */
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));

	  pkt.altitude = tofloat ((BYTE *)(&d->alt));

	  s=(char *)(&d->ident);

	  sprintf(pkt.name, "%-15.15s",s);
	  s+=strlen(s)+1;

	  pkt.comment=s;

	  pkt.seconds=number((BYTE *)(&d->time));
	  pkt.has.altitude=pkt.has.name=pkt.has.comment=pkt.has.seconds=1;
	}
      break;

    case Pid_Trk_Data:
      flag='T';
      if (strstr(protocols, "D300"))
	{
	  D300_Trk_Point_Type *d=(D300_Trk_Point_Type *)(gGarminMessage+3);
	  pkt.seconds = number((BYTE *)(&d->time));
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));
	  pkt.new_trk = d->new_trk;
	  pkt.has.seconds=1;
	}
      if (strstr(protocols, "D301"))
	{
	  D301_Trk_Point_Type *d=(D301_Trk_Point_Type *)(gGarminMessage+3);
	  pkt.seconds = number((BYTE *)(&d->time));
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));
	  pkt.altitude = tofloat ((BYTE *)(&d->alt));
	  pkt.new_trk = d->new_trk;
	  pkt.has.seconds=pkt.has.altitude=1;
	}
      if (strstr(protocols, "D302"))
	{
	  D302_Trk_Point_Type *d=(D302_Trk_Point_Type *)(gGarminMessage+3);
	  pkt.seconds = number((BYTE *)(&d->time));
	  pkt.latitude = int2deg (number ((BYTE *)(&d->posn.lat)));
	  pkt.longitude = int2deg (number ((BYTE *)(&d->posn.lon)));
	  pkt.altitude = tofloat ((BYTE *)(&d->alt));
	  pkt.new_trk = d->new_trk;
	  pkt.has.seconds=pkt.has.altitude=1;
	}
      break;

    }

  /* translate position selected datum */
  translate (1, &pkt.latitude, &pkt.longitude, gPrefs.datum);


  switch(file_format)
    {
    case MAYKO:
    case MAYKO2:
      
      switch(Pid)
	{

	case Pid_Rte_Wpt_Data:
	  sprintf (fileData, "%s,", pkt.name);
	  FileWrite (fileData);
	  
	  sprintf (fileData, "%03.6f,%04.6f,0,,\n", pkt.latitude, pkt.longitude);
	  FileWrite (fileData);
	  break;

	case Pid_Trk_Data:

	  if (pkt.new_trk)
	    {			/* New track, create blank record */
	      xmap_lat = 0;	/* reset xmap lat + lon */
	      xmap_lon = 0;
	      xmap_time = 0;
	      if (records != 0)
		FileWrite ("\n");
	    }

	  /* calculate speed */

	  if (xmap_lat)
	    {
	      x_dist = distance (pkt.latitude, pkt.longitude, xmap_lat, xmap_lon);
	      t_diff = pkt.seconds - xmap_time;
	      if (t_diff)
		x_speed = (float)((x_dist/(double)t_diff)*3600/1.852);
	      else
		x_speed = xmap_speed;
	    }

	  /* write mayko xmap format */

	  sprintf (fileData, "1 %03.7f %04.7f ", pkt.latitude, pkt.longitude);
	  FileWrite (fileData);

	  sprintf (fileData, "%.1f %1.0f ", x_speed, pkt.altitude);
	  FileWrite (fileData);

	  sprintf (fileData, "%s", secs2dt (pkt.seconds, gPrefs.offset));
	  FileWrite (fileData);

	  if (xmap_lat && (file_format == MAYKO2))
	    {
	      sprintf (fileData, " x%03.7f %04.7f", xmap_lat, xmap_lon);
	      FileWrite (fileData);
	      sprintf (fileData, " %06.7f", x_dist);
	      FileWrite (fileData);
	      sprintf (fileData, " -%d-", (int) t_diff);
	      FileWrite (fileData);
	      sprintf (fileData, " %.3f", x_speed);
	      FileWrite (fileData);
	    }

	  FileWrite ("\n");

	  xmap_lat = pkt.latitude; //save lat + lon for next round
	  xmap_lon = pkt.longitude;
	  xmap_time = number (gGarminMessage + 11);
	  xmap_speed = x_speed;
	  break;
	}    
      break;

    case TSV:
      /* write record type flag */
      sprintf (fileData, "%c\t", flag);
      FileWrite (fileData);

      /* write waypoint name */
      if (pkt.has.name)
	{
	  sprintf (fileData, "%s\t", pkt.name);
	  FileWrite (fileData);
	}
  
      /* write comment - up to 40 characters */
      if (pkt.has.comment)
	{
	  sprintf (fileData, "%-40s\t", pkt.comment);
	  FileWrite (fileData);
	}
  
      /* write date/time */
      if (pkt.has.seconds)
	{
	  sprintf (fileData, "%s\t", secs2dt (pkt.seconds, gPrefs.offset));
	  FileWrite (fileData);
	}
  
      /* write altitude */
      if (pkt.has.altitude) /* does the packet accommodate altitude? */
	{
	  if (pkt.altitude < 1.e24)	/* is the altitude valid? */
	    if (gPrefs.feet)
	      sprintf (fileData, "%9.2f ft\t", pkt.altitude/.3048);
	    else
	      sprintf (fileData, "%9.2f m\t", pkt.altitude);
	  else
	    sprintf (fileData, "unknown  \t");
	  FileWrite (fileData);
	}


      switch (gPrefs.format)
	{
	case DMS:
	  sprintf (fileData, "%s\t", toDMS (pkt.latitude));
	  FileWrite (fileData);
	  sprintf (fileData, "%s\n", toDMS (pkt.longitude));
	  FileWrite (fileData);
	  break;
	case DMM:
	  sprintf (fileData, "%s\t", toDM (pkt.latitude));
	  FileWrite (fileData);
	  sprintf (fileData, "%s\n", toDM (pkt.longitude));
	  FileWrite (fileData);
	  break;
	case DDD:
	  sprintf (fileData, "%03.7f\t%04.7f\n", pkt.latitude, pkt.longitude);
	  FileWrite (fileData);
	  break;
	case UTM:			/* convert to UTM grid */
	  DegToUTM (pkt.latitude, pkt.longitude, zone, &x, &y);
	  sprintf (fileData, "%s\t%07.0f\t%07.0f\n", zone, x, y);
	  FileWrite (fileData);
	  break;
	case KKJ:			/* convert to KKJ grid */
	  DegToKKJ (pkt.latitude, pkt.longitude, zone, &x, &y);
	  sprintf (fileData, "%s\t%07.0f\t%07.0f\n", zone, x, y);
	  FileWrite (fileData);
	  break;
	case BNG:			/* convert to British grid */
	  DegToBNG (pkt.latitude, pkt.longitude, zone, &x, &y);
	  sprintf (fileData, "%s\t%05.0f\t%05.0f\n", zone, x, y);
	  FileWrite (fileData);
	  break;
	case ITM:			/* convert to irish grid */
	  DegToITM (pkt.latitude, pkt.longitude, zone, &x, &y);
	  sprintf (fileData, "%s\t%05.0f\t%05.0f\n", zone, x, y);
	  FileWrite (fileData);
	  break;
	case SEG:			/* convert to Swedish grid */
	  DegToSEG (pkt.latitude, pkt.longitude, &x, &y);
	  sprintf (fileData, "%07.0f\t%07.0f\n", x, y);
	  FileWrite (fileData);
	  break;
	case GKK:			/* convert to German Grid */
	  DegToGKK (pkt.latitude, pkt.longitude, &x, &y);
	  sprintf (fileData, "%07.0f\t%07.0f\n", x, y);
	  FileWrite (fileData);
	  break;
	default:
	  break;
	}
      break;
    }
}



/****************************************************************************/
/* Process Almanac package (D500, D501, D550, or D551) and write to
   output file\.  */
/****************************************************************************/
static void
doAlmanac ()
{
  char temp[255];

  typedef struct
  {
    int svid;
    short wn;
    double toa, af0, af1, e, sqrta, m0, w, omg0, odot, i;
    int hlth;
  } almanac_type;
  almanac_type pkt;

  pkt.svid = pkt.hlth = ~1;

  if (strstr(protocols, "D500"))
    {
      D500_Almanac_Type *d=(D500_Almanac_Type *)(gGarminMessage+3);

      /* no satellite ID */
      pkt.wn    = toshort ((BYTE *)(&d->wn   ));
      pkt.toa   = tofloat ((BYTE *)(&d->toa  ));
      pkt.af0   = tofloat ((BYTE *)(&d->af0  ));
      pkt.af1   = tofloat ((BYTE *)(&d->af1  ));
      pkt.e     = tofloat ((BYTE *)(&d->e    ));
      pkt.sqrta = tofloat ((BYTE *)(&d->sqrta));
      pkt.m0    = tofloat ((BYTE *)(&d->m0   ));
      pkt.w     = tofloat ((BYTE *)(&d->w    ));
      pkt.omg0  = tofloat ((BYTE *)(&d->omg0 ));
      pkt.odot  = tofloat ((BYTE *)(&d->odot ));
      pkt.i     = tofloat ((BYTE *)(&d->i    ));
      /* no hlth */
    }
  else if (strstr(protocols, "D501"))
    {
      D501_Almanac_Type *d=(D501_Almanac_Type *)(gGarminMessage+3);

      /* no satellite ID */
      pkt.wn    = toshort ((BYTE *)(&d->wn   ));
      pkt.toa   = tofloat ((BYTE *)(&d->toa  ));
      pkt.af0   = tofloat ((BYTE *)(&d->af0  ));
      pkt.af1   = tofloat ((BYTE *)(&d->af1  ));
      pkt.e     = tofloat ((BYTE *)(&d->e    ));
      pkt.sqrta = tofloat ((BYTE *)(&d->sqrta));
      pkt.m0    = tofloat ((BYTE *)(&d->m0   ));
      pkt.w     = tofloat ((BYTE *)(&d->w    ));
      pkt.omg0  = tofloat ((BYTE *)(&d->omg0 ));
      pkt.odot  = tofloat ((BYTE *)(&d->odot ));
      pkt.i     = tofloat ((BYTE *)(&d->i    ));
      pkt.hlth  =                    d->hlth;
    }
  else if (strstr(protocols, "D550"))
    {
      D550_Almanac_Type *d=(D550_Almanac_Type *)(gGarminMessage+3);

      pkt.svid  =                    d->svid;
      pkt.wn    = toshort ((BYTE *)(&d->wn   ));
      pkt.toa   = tofloat ((BYTE *)(&d->toa  ));
      pkt.af0   = tofloat ((BYTE *)(&d->af0  ));
      pkt.af1   = tofloat ((BYTE *)(&d->af1  ));
      pkt.e     = tofloat ((BYTE *)(&d->e    ));
      pkt.sqrta = tofloat ((BYTE *)(&d->sqrta));
      pkt.m0    = tofloat ((BYTE *)(&d->m0   ));
      pkt.w     = tofloat ((BYTE *)(&d->w    ));
      pkt.omg0  = tofloat ((BYTE *)(&d->omg0 ));
      pkt.odot  = tofloat ((BYTE *)(&d->odot ));
      pkt.i     = tofloat ((BYTE *)(&d->i    ));
      /* no hlth */
    }
  else if (strstr(protocols, "D551"))
    {
      D551_Almanac_Type *d=(D551_Almanac_Type *)(gGarminMessage+3);
      pkt.svid  =                    d->svid;
      pkt.wn    = toshort ((BYTE *)(&d->wn   ));
      pkt.toa   = tofloat ((BYTE *)(&d->toa  ));
      pkt.af0   = tofloat ((BYTE *)(&d->af0  ));
      pkt.af1   = tofloat ((BYTE *)(&d->af1  ));
      pkt.e     = tofloat ((BYTE *)(&d->e    ));
      pkt.sqrta = tofloat ((BYTE *)(&d->sqrta));
      pkt.m0    = tofloat ((BYTE *)(&d->m0   ));
      pkt.w     = tofloat ((BYTE *)(&d->w    ));
      pkt.omg0  = tofloat ((BYTE *)(&d->omg0 ));
      pkt.odot  = tofloat ((BYTE *)(&d->odot ));
      pkt.i     = tofloat ((BYTE *)(&d->i    ));
      pkt.hlth  =                    d->hlth;
    }



  if (pkt.svid == ~1)
    pkt.svid = ++AlmanacSat;	/* next satellite */

  /* don't handle if satellite not active

  "If the data for a particular satellite is missing or if the
  satellite is non-existent, then the week number for that satellite
  must be set to a negative number to indicate that the data is
  invalid." */
  if (pkt.wn < 0)
    return;

  /* print almanac data in yuma-format */
  sprintf (temp, "******** Week %d almanac for PRN-%02d ********\n",
	   pkt.wn, pkt.svid);
  FileWrite (temp);
  sprintf (temp, "ID:                         %02d\n", pkt.svid);
  FileWrite (temp);
  sprintf (temp, "Health:                     %03d\n", pkt.hlth);
  FileWrite (temp);
  sprintf (temp, "Eccentricity:              % 1.10E\n", pkt.e);
  FileWrite (temp);
  sprintf (temp, "Time of Applicability(x):  %10.2f\n", pkt.toa);
  FileWrite (temp);
  sprintf (temp, "Orbital Inclination(rad):  % 1.10f\n", pkt.i);
  FileWrite (temp);
  sprintf (temp, "Rate of Right Ascen(r/s):  % 1.10E\n", pkt.odot);
  FileWrite (temp);
  sprintf (temp, "SQRT(A)  (m^1/2):          % 1.10f\n", pkt.sqrta);
  FileWrite (temp);
  sprintf (temp, "Right Ascen at TOA(rad):   % 1.10E\n", pkt.omg0);
  FileWrite (temp);
  sprintf (temp, "Argument of Perigee(rad):  % 1.10f\n", pkt.w);
  FileWrite (temp);
  sprintf (temp, "Mean Anom(rad):            % 1.10E\n", pkt.m0);
  FileWrite (temp);
  sprintf (temp, "Af0(s):                    % 1.10E\n", pkt.af0);
  FileWrite (temp);
  sprintf (temp, "Af1(s/s):                  % 1.10E\n", pkt.af1);
  FileWrite (temp);
  sprintf (temp, "week:                       %d\n",     pkt.wn);
  FileWrite (temp);
  sprintf (temp, "\n");
  FileWrite (temp);

}


/****************************************************************************/
/* Convert bytes to string.                                                 */
/****************************************************************************/
static char *
string (short n, BYTE * p)
{
  static char s[50];
  short i;

  for (i = 0; i < n; i++)
    s[i] = *p++;
  s[n] = '\0';

  return s;
}


/****************************************************************************/
/* Convert bytes to long number.                                            */
/****************************************************************************/
static long
number (BYTE * p)
{
  long n;
  /* LSB is first */
  n = ((((((long) p[3] << 8) + p[2]) << 8) + p[1]) << 8) + p[0];

  return n;
}


/****************************************************************************/
/* Convert bytes to float number.                                           */
/****************************************************************************/
static float
tofloat (BYTE * p)
{
  float n;
  long *lp = (long *) &n;
  /* LSB is first */
  *lp = ((((((long) p[3] << 8) + p[2]) << 8) + p[1]) << 8) + p[0];
  return n;
}


/****************************************************************************/
/* Convert bytes to short number.                                           */
/****************************************************************************/
static short
toshort (BYTE * p)
{
  short n;

  n = ((short) p[1] << 8) + p[0];

  return n;
}

/****************************************************************************/
/* Write string to output file\.                                           */
/****************************************************************************/
static void
FileWrite (char *data)
{
  fputs (data, FileRefNum);
}
