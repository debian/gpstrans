/****************************************************************************/
/*                                                                          */
/* ./gps/gpsmessage.c   -   Define GPS commands                             */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"



/****************************************************************************/
/* Define Garmin-Protocol commands.                                         */
/****************************************************************************/
BYTE m1[] = "\xfe\x01\x20";	/* Get the model and version number */
BYTE m2[] = "\x06\x02\xff\x00";	/* unknown */

BYTE p1[] = "\x06\x02\x1b\x00";	/* Get 1st packet */
BYTE p2[] = "\x06\x02\x0c\x00";	/* Get last packet */

BYTE alm1[] = "\x0a\x02\x01\x00";	/* Send almanac command */
BYTE alm2[] = "\x06\x02\x1f\x00";	/* Acknowlege almanac packet */

BYTE trk1[] = "\x0a\x02\x06\x00";	/* Get track command */
BYTE trk2[] = "\x06\x02\x22\x00";	/* Acknowlege track packet */

BYTE wpt1[] = "\x0A\x02\x07\x00";	/* Get waypoint command */
BYTE wpt2[] = "\x06\x02\x23\x00";	/* Acknowlege waypoint packet */

BYTE rte1[] = "\x0a\x02\x04\x00";	/* Get route command */
BYTE rte2[] = "\x06\x02\x1d\x00";	/* Acknowlege route packet */

BYTE test[] = "\x1c\x02\x00\x00";	/* Get response from Garmin */

BYTE gid4[] = "\xfe\x00";	/* Get Garmin model & Software rev */
BYTE gid5[] = "\x06\x02\xff\x00";

BYTE off1[] = "\x0a\x02\x08\x00";	/* Turn Garmin Off */
BYTE tim1[] = "\x0a\x02\x05\x00";	/* Get UTC Time */

BYTE almt[] = "\x0c\x02\x01\x00";	/* Almanac data base terminator */
BYTE rtet[] = "\x0c\x02\x04\x00";	/* Route data base terminator */
BYTE trkt[] = "\x0c\x02\x06\x00";	/* Track data base terminator */
BYTE wptt[] = "\x0c\x02\x07\x00";	/* Waypoint data base terminator */
