#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <sys/ioctl.h>
#include <unistd.h>


#define ProgramName          "GPStrans"
#define ProgramVersion       "0.41"
#define ProgramYear          "2005"

#define MainMenuHeader       "GPS Transfer"

#define BarWindowHeader      "Transfer in progress..."
#define PrefsWindowHeader    "Preferences"

#define PrefFileName         "~/.gpstrans"

#define DefaultServerDevice  "/dev/ttyS1"
#define ServerBaud           B4800

#define TIMEOUT              10

#define noErr		      0

#define X_NULL               (int) NULL
