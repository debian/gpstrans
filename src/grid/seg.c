/****************************************************************************/
/*                                                                          */
/* ./grid/seg.c   -   Convert to Swedish Grid based on the datum RT90       */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/* Copyright (c) 1999 by Anders Lennartsson (anders.lennartsson@sto.foa.se) */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"
#include <math.h>

/* define constants */
static const double lat0 = 0.0;	/* reference transverse mercator latitude */
				  /* for SEG: the equator of Bessel's 1841 ellipsoid */
static const double lon0 = 15.808277777778;	/* reference longitude */
				  /* for SEG: 2.5 gon east of Stockholm's old observatory  */
static const double k0 = 1.0;

/****************************************************************************/
/* Convert degree to Swedish Grid Format.                                   */
/****************************************************************************/
void
DegToSEG (double lat, double lon, double *x, double *y)
{
  toTM (lat, lon, lat0, lon0, k0, x, y);

  /* add false easting and round to nearest meter */
  *y = floor (*y + 0.5);
  *x = floor (*x + 1500000.5);
  /* realistic values */
  /* 6100000m < x < 7700000m */
  /* 1200000m < y < 1900000m */
}


/****************************************************************************/
/* Convert Swedish Grid Format to degree.                                   */
/****************************************************************************/
void
SEGtoDeg (double x, double y, double *lat, double *lon)
{
  x -= 1500000.0;

  fromTM (x, y, lat0, lon0, k0, lat, lon);
}
