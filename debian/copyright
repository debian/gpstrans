Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gpstrans
Upstream-Contact: Jim R. Van Zandt <jrv@debian.org>
Source: ftp://ftp.fu-berlin.de/pub/unix/misc/gps

Files: *
Copyright: 1995      Carsten tschach <tschach@zedat.fu-berlin.de>
           1995      John F. Waers <jfwaers@csn.net>
           1996      Janne Sinkkonen <janne@iki.fi>
           1998-2000 Matthias Kattanek <mattes@ugraf.com>
           1999      Anders Lennartsson <anders.lennartsson@sto.foa.se>	
           2000      Andeas Lange <andreas.lange@rhein-main.de>
           2001      Joao Seabra - CT2GNL <seabra@ci.aac.uc.pt>
           2005-2008 Jim R. Van Zandt <jrv@debian.org>
License: GPL-2+
Comment: since 0.39 version GPStrans is under GPL-2. From upstream changelog:
         "Changed code to GNU GPL (with Carsten Tschach's permission). This
         and future releases will be totally under GNU GPL.". The statements
         in headers say about GPL-2+.

Files: src/getline/getline.c
Copyright: 1991-1993 Chris Thewalt <thewalt@ce.berkeley.edu>
License: special

Files: debian/*
Copyright: 1997-2010 Jim R. Van Zandt <jrv@debian.org>
           2008      Petter Reinholdtsen <pere@debian.org>
           2010      Francesco Paolo Lovergine <frankie@debian.org>
           2014      Emanuele Rocca <ema@debian.org>
           2015      Raphael Mota Ramos <raphaelmota.ti@gmail.com>
           2016      Andreas Beckmann <anbe@debian.org>
           2020      Carlos Henrique Lima Melara <charlesmelara@outlook.com>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, 
 Boston, MA  02110-1301, USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: special
 Permission to use, copy, modify, and distribute this software 
 for any purpose and without fee is hereby granted, provided
 that the above copyright notices appear in all copies and that both the
 copyright notice and this permission notice appear in supporting
 documentation.  This software is provided "as is" without express or
 implied warranty.
