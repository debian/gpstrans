gpstrans (0.41-11) unstable; urgency=medium

  * QA upload.

  * Updated 1070-64bit-time.patch to actually fix armhf build failure.

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 07 Aug 2024 18:50:41 +0200

gpstrans (0.41-10) unstable; urgency=medium

  * QA upload.

  * Use time_t as ctime() argument to use 64 bit time on 32 bit platforms
    (Closes: #1078085).
  * Introduced patch numbering scheme.
  * Updated Standards-Version from 4.5.1 to 4.7.0.
  * Adjusted metadata of 1030-fix-makefile.diff.

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 07 Aug 2024 08:13:52 +0200

gpstrans (0.41-9) unstable; urgency=medium

  * QA upload.

  * Corrected 1000-appstream-metainfo.patch to avoid errors from appstream.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 05 Aug 2024 22:52:19 +0200

gpstrans (0.41-8) unstable; urgency=medium

  * QA upload.

  * Added 1000-appstream-metainfo.patch with AppStream metainfo XML
    announcing supported hardware.
  * Added d/gbp.conf to enforce use of pristine-tar and specify
    used branches.
  * Added 1010-gl-in-hook-prototype.patch to ensure consistent
    gl_in_hook declarations (Closes: #688452).

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 20 Jul 2024 12:25:59 +0200

gpstrans (0.41-7) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Added Vcs-* fields pointing to Salsa.
      - Bumped Standards-Version to 4.5.1.
      - Changed Priority from extra to optional.
      - Changed the short description to reflect the use of serial port
        communication. (Closes: #932748)
  * debian/copyright:
      - Fixed a typo in Comment field.
      - Updated packaging copyright.
      - Using secure URI.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to provide CI tests for the package.
  * debian/watch: bumped version to 4.

 -- Carlos Henrique Lima Melara <charlesmelara@outlook.com>  Sun, 24 Jan 2021 20:13:41 -0300

gpstrans (0.41-6) unstable; urgency=medium

  * QA upload.
  * 05-libgetline-re-enable-posix-macro.diff: New, add missing -DPOSIX for
    getline, thanks to Jurica Stanojkovic.  (Closes: #818298)
  * Bumped Standards-Version to 3.9.7.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 19 Mar 2016 15:49:11 +0100

gpstrans (0.41-5) unstable; urgency=medium

  * QA upload.
  * Bumped Standards-Version to 3.9.6.
  * debian/control: added the original upstream homepage.
  * debian/copyright:
     - Migrated to 1.0 format.
     - Updated all data.
  * debian/Makefile.patch: removed because this file is useless.
  * debian/patches: created the 04-fix-makefile.diff to improve the GCC
    hardening.
  * debian/rules: added the DEB_BUILD_MAINT_OPTIONS variable to improve
    the GCC hardening.
  * debian/watch: added a fake site to explain about the current status
    of the original upstream homepage.

 -- Raphael Mota Ramos <raphaelmota.ti@gmail.com>  Tue, 01 Dec 2015 07:58:10 -0200

gpstrans (0.41-4) unstable; urgency=low

  * QA upload.
  * Maintainer field set to QA Group.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Add debian/patches/03-src-makefile-flags.diff to avoid overwriting CFLAGS
    and LDFLAGS in src/Makefile.
  * Rewrite debian/rules using dh(1).
  * Set debhelper compatibility level to 9.
  * Bump Standards-Version to 3.9.5.
  * Add debian/gpstrans.manpages.

 -- Emanuele Rocca <ema@debian.org>  Fri, 28 Mar 2014 13:43:14 +0100

gpstrans (0.41-3) unstable; urgency=low

  * debian/control: bump debian policy number to 3.8.4 (no changes
    needed).  Add dependency "${misc:Depends}".

 -- James R. Van Zandt <jrv@debian.org>  Sun, 07 Feb 2010 10:29:42 -0500

gpstrans (0.41-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Changed getline() in my_getline() to avoid eglibc colliding names in getline.c.
    (closes: #552864)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Wed, 27 Jan 2010 17:18:46 +0100

gpstrans (0.41-2) unstable; urgency=low

  * suggest setserial (thanks to Joerg Schuetter <joerg@schuetter.org>,
    closes:Bug#513137)

  * debian/copyright: mention that GPL can be found in
    /usr/share/common-licenses, to silence lintian complaint.

 -- James R. Van Zandt <jrv@debian.org>  Sat, 14 Feb 2009 10:08:23 -0500

gpstrans (0.41-1) unstable; urgency=low

  * New upstream release: incorporates fix for segfault identified by
    Robie Basak (#399828)
  * debian/watch: delete, since I am the upstream maintainer (closes:Bug#449884)
  * debian/dirs: don't include usr/sbin
  * debian/rules: obey DEBIAN_BUILD_OPTIONS "nostrip" flag
  * debian/control: bump debian policy number to 3.8.0

 -- James R. Van Zandt <jrv@debian.org>  Wed, 16 Jul 2008 20:09:58 -0400

gpstrans (0.40-3.1) unstable; urgency=low

  * Non-maintainer upload to fix RC bug.
  * Fix segfault using patch from Robie Basak (Closes: #399828).

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 30 Mar 2008 23:38:02 +0200

gpstrans (0.40-3) unstable; urgency=low

  * don't always strip the binary (closes:Bug#437077)
  * don't ignore failures in "make clean" (silence lintian warning)
  * bump standards level to 3.7.2 (no changes needed)

 -- James R. Van Zandt <jrv@debian.org>  Fri, 10 Aug 2007 21:50:08 -0400

gpstrans (0.40-2) unstable; urgency=low

  * debian/rules: #define POSIX and unix, for compilation of getline on
    alpha, mips, and mipsel.
  * debian/foo: delete extraneous file.

 -- James R. Van Zandt <jrv@debian.org>  Tue,  9 May 2006 21:21:19 -0400

gpstrans (0.40-1) unstable; urgency=low

  * New upstream release: understands most (all?) Garmin formats
    (Closes:#256253), exit with nonzero status if GPS doesn't respond
    (Closes:#218137)
  * debian/rules: don't make /usr/doc symlink (Closes: #359413), advance
    to debhelper version 5.  Use Debian advised CFLAGS.
  * debian/control: Depend on debhelper 5.  Policy version 3.6.2.

 -- James R. Van Zandt <jrv@debian.org>  Mon,  8 May 2006 21:11:15 -0400

gpstrans (0.39-3) unstable; urgency=low

  * omit old README.debian file

 -- James R. Van Zandt <jrv@debian.org>  Sat, 16 Nov 2002 16:45:37 -0500

gpstrans (0.39-2) unstable; urgency=low

  * debian/: remove *.ex files
  * src/grid/utm.c: sign error in northing calculation (thanks to James
    Cameron <quozl@us.netrek.org>, closes:Bug#140786)

 -- James R. Van Zandt <jrv@debian.org>  Fri, 12 Apr 2002 20:45:10 -0400

gpstrans (0.39-1) unstable; urgency=low

  * new upstream version
  * convert to debhelper
  * add menu entry

 -- James R. Van Zandt <jrv@debian.org>  Sat,  5 Jan 2002 19:54:54 -0500

gpstrans (0.38-5) unstable; urgency=low

  * debian/control: spelling fix (Closes:Bug#124698)

 -- James R. Van Zandt <jrv@debian.org>  Fri, 21 Dec 2001 20:00:15 -0500

gpstrans (0.38-4) unstable; urgency=low

  * remove build-depend on cpp-2.95 since gcc depends on it
    (closes:bug#107649,bug#108082)

 -- James R. Van Zandt <jrv@debian.org>  Thu,  9 Aug 2001 22:19:04 -0400

gpstrans (0.38-3) unstable; urgency=low

  * build-depends on cpp rather than cpp-2.95 (closes:bug#107649)

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sun,  5 Aug 2001 18:04:11 -0400

gpstrans (0.38-2) unstable; urgency=low

  * doc/gpstrans.1 document -z option (Closes:bug#106080)
  * fix spelling of "transferred" four places (Closes:bug#106085)
  * more build-dependencies
  * new maintainer email

 -- James R. Van Zandt <jrv@debian.org>  Sat, 21 Jul 2001 15:57:31 -0400

gpstrans (0.38-1) unstable; urgency=low

  * new upstream version
  * debian/copyright: new upstream maintainer, new home site
  * debian/watch: point to new upstream home http://www.aac.uc.pt/~seabra/
  * debian/control: move Build-Depends line to source section, policy
    version 3.5.2
  * src/prefs.c: change default model to 'n' (i.e. not etrex or similar)
    to match previous releases.
  * debian/rules: honor DEB_BUILD_OPTIONS

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sat,  7 Apr 2001 09:36:52 -0400

gpstrans (0.34-8) unstable; urgency=low

  * build-depends on debmake

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sun,  3 Sep 2000 11:51:59 -0400

gpstrans (0.34-7) unstable; urgency=low

  * gps/getgpsinfo.c: quote pi to only 16 digits (the next 4 were
    incorrect anyway :-)

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Tue,  2 Nov 1999 21:26:00 -0500

gpstrans (0.34-6) unstable; urgency=low

  * Add /usr/doc/gpstrans symlink.

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Tue, 14 Sep 1999 21:43:29 -0400

gpstrans (0.34-5) unstable; urgency=low

  * link against glibc2.1
  * update to FHS

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Thu,  2 Sep 1999 19:40:38 -0400

gpstrans (0.34-4) unstable; urgency=low

  * include -DPOSIX in CFLAGS, as required by alpha (at least) (Bug#39293)

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Thu, 17 Jun 1999 21:18:54 -0400

gpstrans (0.34-3) unstable; urgency=low

  * Default port is now /dev/ttyS1 instead of /dev/cua1 (bug#27147)

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Mon, 21 Dec 1998 21:36:26 -0500

gpstrans (0.34-2) unstable; urgency=low

  * Fixed name in man page: "Mayco" -> "Mayko"

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Mon, 17 Aug 1998 15:36:51 -0400

gpstrans (0.34-1) unstable; urgency=low

  * New upstream release

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sun, 16 Aug 1998 17:37:38 -0400

gpstrans (0.33-mk-1) unstable; urgency=low

  * Initial Release.  Former Debian patches have been incorporated into
    the upstream sources.

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Fri,  7 Aug 1998 19:37:56 -0400

gpstrans (0.32-1) unstable; urgency=low

  * new upstream maintainer: Matthias Kattanek <mattes@ugraf.com>,
    new upstream version.

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sun, 28 Jun 1998 13:34:19 -0400

gpstrans (0.31b-5) frozen unstable; urgency=low

  * Increased maximum record length read from device, to suit Garmin 90
    (bug reported by Klaus Wacker <wacker@Physik.Uni-Dortmund.DE>)
  * Maximum length line read from data file now limited to size of
    available buffer.
  * Updated policy version.

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sun,  5 Apr 1998 18:41:12 -0400

gpstrans (0.31b-4) unstable; urgency=low

  * remove acronym from short description, reword long description in
    control file and manual page

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sun, 15 Feb 1998 21:32:39 -0500

gpstrans (0.31b-3) unstable; urgency=low

  * eliminate extra files in debian directory
  * update policy version
  * change priority to "extra" since having a Garmin GPS is a "specialized
    requirement"

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Wed, 11 Feb 1998 19:12:14 -0500

gpstrans (0.31b-2) unstable; urgency=low

  * link against libc6
  * eliminate extraneous hunks in .diff file (#11265)

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Sat, 2 Aug 1997 12:12:17 -0400

gpstrans (0.31b-1) unstable; urgency=low

  * ascii/gps/garminserial.c: setting CLOCAL, so /dev/ttyS? works
    (formerly, only /dev/cua? worked)
  * ascii/main.c: Modified byte order conversions to work correctly on
    either big endian or little endian machine.
  * The format for a time should be 0.
  * fix some English spelling and usage errors.
  * latlong.c: Under Linux, PI is alreading #defined in math.h.
  * gpstrans.1: Added circuit diagram, added highlighting, noted that
    -s saves the values in ~/.gpstrans.
  * Not including getline.3 man page in the binary package, since no
    library is included
  * Initial Release.

 -- James R. Van Zandt <jrv@vanzandt.mv.com>  Wed, 9 Apr 1997 20:24:01 -0400
