##############################################################################
#
#                Makefile for the ascii-version of GPStrans
#
##############################################################################

PACKAGE=gpstrans

all: 
	@make -C src/

clean:
	@echo "Cleaning all in ./"
	@rm -f src/core src/gpstrans src/*.o src/*.bak src/*~ src/#*#
	@echo "Cleaning all in ./gps"
	@(cd src/gps  ; make clean)
	@echo "Cleaning all in ./grid"
	@(cd src/grid ; make clean)
	@echo "Cleaning all in ./getline"
	@(cd src/getline ; make clean)

install:
	@strip src/gpstrans
	@cp src/gpstrans debian/gpstrans/usr/bin/gpstrans
#	@gzip -9 doc/gpstrans.1 > /usr/share/man/man1/gpstrans.1.gz
	@echo "Installation complete!"

make uninstall:

	@rm -f /usr/local/bin/gpstrans
	@rm -f /usr/share/man/man1/gpstrans.1.gz
	@rm -f ~/.gpstrans
	@echo "Uninstall complete!"


DIRS=	doc					\
	src					\
	src/gps					\
	src/grid				\
	src/getline				\
	src/include

DISTFILES=COPYING				\
	ChangeLog				\
	INSTALL					\
	LICENSE					\
	Makefile				\
	README					\
	README.mxmap				\
	TODO					\
	copyright				\
	doc/gpstrans-old.html			\
	doc/gpstrans.1				\
	src/Makefile				\
	src/TAGS				\
	src/graph.c				\
	src/main.c				\
	src/prefs.c				\
	src/util.c				\
	src/getline/CHANGES			\
	src/getline/Makefile			\
	src/getline/README			\
	src/getline/getline.3			\
	src/getline/getline.c			\
	src/getline/getline.h			\
	src/getline/testgl.c			\
	src/gps/Makefile			\
	src/gps/calendar.c			\
	src/gps/datum.c				\
	src/gps/dms.c				\
	src/gps/garmincomm.c			\
	src/gps/garminserial.c			\
	src/gps/getgpsinfo.c			\
	src/gps/gpsmessage.c			\
	src/gps/latlong.c			\
	src/gps/sendgpsinfo.c			\
	src/grid/Makefile			\
	src/grid/bng.c				\
	src/grid/gkk.c				\
	src/grid/gridutils.c			\
	src/grid/itm.c				\
	src/grid/kkj.c				\
	src/grid/seg.c				\
	src/grid/tm.c				\
	src/grid/ups.c				\
	src/grid/utm.c				\
	src/include/Garmin.h			\
	src/include/Prefs.h			\
	src/include/defs.h			\
	src/include/gpsinfo.h			\
	src/include/gpstrans.gif		\
	src/include/gpstrans.icon		\
	src/include/gpstrans_small.gif		\
	src/include/graph.h			\
	src/include/icon.icon			\
	src/include/main.gif			\
	src/include/protocols.h			\
	src/include/util.h

srcdir = .
top_srcdir = .
top_distdir = .
VERSION := $(shell perl -ne 'if (/ProgramVersion.*"([0-9.]*)"/){ print "$$1";}' src/include/defs.h)
distdir := $(PACKAGE)-$(VERSION)

distdir: $(DISTFILES)
	rm -fr $(distdir)
	$(am__remove_distdir)
	mkdir $(distdir)
	for d in $(DIRS); do mkdir $(distdir)/$$d; chmod 755 $(distdir)/$$d; done
	for f in $(DISTFILES); do cp -p $$f $(distdir)/$$f; done
dist: distdir
	tar chof - $(distdir) | GZIP=--best gzip -c >$(distdir).tar.gz
	rm -fr $(distdir)
